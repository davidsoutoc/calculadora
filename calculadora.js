'use strict';

require('dotenv').config();
const args = require('minimist')(process.argv.slice(2));
const { suma, resta } = require('./lib/operations');
const translations = require('./lib/translations');

const { LANGUAGE } = process.env;

//const txtTranslations = translations[LANGUAGE];
const { error1, error2, txtResult } = translations[LANGUAGE];
const validOperations = ['suma', 'resta'];

const { operation, valorA, valorB } = args;
if (!operation || !valorA || !valorB) {
  console.log(error1);
  process.exit();
}

if (!validOperations.includes(operation)) {
  console.log(error2);
  process.exit();
}

let result;
switch (operation) {
  case "suma":
    result = suma(valorA, valorB);
    break;
  case "resta":
    result = resta(valorA, valorB);
    break;
}

console.log(`${txtResult} ${result}`);

