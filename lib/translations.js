'use strict';

const es = {
  error1: 'Faltan parámetros',
  error2: 'Operación no válida',
  txtResult: 'El resultado es',
}

const en = {
  error1: 'Missing parameters',
  error2: 'The operation is not valid',
  txtResult: 'The result is',
}

module.exports = {
  es,
  en,
}