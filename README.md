# Descripción proyecto

Calculadora por terminal
El numero de operaciones son: suma, resta, multiplica, divide

## Ejemplos de uso

node calculadora.js --operation suma --valueA 10 --valueB 20
node calculadora.js --valueB 20 --valueA 10 --operation suma
node calculadora.js --valueA 10 --operation suma --valueB 20
